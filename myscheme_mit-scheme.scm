;; 4.1.1 評価機のコア

;; eval
(define (eval exp env)
  (cond ((self-evaluating? exp) exp)
        ((variable? exp) (lookup-variable-value exp env))
        ((quoted? exp) (text-of-quotation exp))
        ((assignment? exp) (eval-assignment exp env))
        ((definition? exp) (eval-definition exp env))
        ((if? exp) (eval-if exp env))
        ; 練習問題 4.4
        ((and? exp) (eval-and exp env))
        ((or? exp) (eval-or exp env))
        ; 練習問題 4.6
        ((let? exp) (eval (let->combination exp) env))
        ; 練習問題 4.7
        ((let*? exp) (eval (let*->nested-lets exp) env))
        ; 練習問題 4.9
        ((do? exp) (eval (do->let exp) env))
        ; 練習問題 4.13
        ((unbound? exp) (eval-unbound exp env))
        ((lambda? exp) (make-procedure (lambda-parameters exp)
                                       (lambda-body exp)
                                       env))
        ((begin? exp)
         (eval-sequence (begin-actions exp) env))
        ((cond? exp) (eval (cond->if exp) env))
        ((application? exp)
         (apply (eval (operator exp) env)
                (list-of-values (operands exp) env)))
        (else
         (error "Unknown expression type: EVAL" exp))))

;; apply
(define apply-in-underlying-scheme apply)

(define (apply procedure arguments)
  (cond ((primitive-procedure? procedure)
         (apply-primitive-procedure procedure arguments))
        ((compound-procedure? procedure)
         (eval-sequence
          (procedure-body procedure)
          (extend-environment
           (procedure-parameters procedure)
           arguments
           (procedure-environment procedure))))
        (else
         (error
          "Unknown procedure type: APPLY" procedure))))

;; 手続きの引数
;; (define (list-of-values exps env)
;;   (if (no-operands? exps)
;;       '()
;;       (cons (eval (first-operand exps) env)
;;             (list-of-values (rest-operands exps) env))))

;; 練習問題 4.1
;; 必ず左から適用するようにする
(define (list-of-values exps env)
  (if (no-operands? exps)
      '()
      (let ((first-eval (eval (first-operand exps) env))
            (second-eval (list-of-values (rest-operands exps) env)))
        (cons first-eval second-eval))))

;; 条件文
(define (eval-if exp env)
  (if (true? (eval (if-predicate exp) env))
      (eval (if-consequent exp) env)
      (eval (if-alternative exp) env)))

;; 列
(define (eval-sequence exps env)
  (cond ((last-exp? exps)
         (eval (first-exp exps) env))
        (else
         (eval (first-exp exps) env)
         (eval-sequence (rest-exps exps) env))))

;; 代入と定義
(define (eval-assignment exp env)
  (set-variable-value! (assignment-variable exp)
                       (eval (assignment-value exp) env)
                       env)
  'ok)

(define (eval-definition exp env)
  (define-variable! (definition-variable exp)
                    (eval (definition-value exp) env)
                    env)
  'ok)

;; 練習問題 4.4
(define (and? exp) (tagged-list? exp 'and))
(define (and-first-exp exp) (cadr exp))
(define (and-second-exp exp) (caddr exp))
(define (eval-and exp env)
  (if (true? (eval (and-first-exp exp) env))
      (if (true? (eval (and-second-exp exp) env))
          'true
          'false)
      'false))

(define (or? exp) (tagged-list? exp 'or))
(define (or-first-exp exp) (cadr exp))
(define (or-second-exp exp) (caddr exp))
(define (eval-or exp env)
  (if (true? (eval (or-first-exp exp) env))
      'true
      (if (true? (eval (or-second-exp exp) env))
          'true
          'false)))

;; 4.1.2 式の表現
;; 自己評価式は数値と文字列しかありません。
(define (self-evaluating? exp)
  (cond ((number? exp) true)
        ((string? exp) true)
        (else false)))

;; 変数は記号によって表現されます。
(define (variable? exp) (symbol? exp))

;; クォート式は (quote <text-of-quotation>) という形式です。
(define (quoted? exp) (tagged-list? exp 'quote))
(define (text-of-quotation exp) (cadr exp))
(define (tagged-list? exp tag)
  (if (pair? exp)
      (eq? (car exp) tag)
      false))

;; 代入は (set! <var> <value>) という形式です。
(define (assignment? exp) (tagged-list? exp 'set!))
(define (assignment-variable exp) (cadr exp))
(define (assignment-value exp) (caddr exp))

;; 定義
(define (definition? exp) (tagged-list? exp 'define))
(define (definition-variable exp)
  (if (symbol? (cadr exp))
      (cadr exp)
      (caadr exp)))
(define (definition-value exp)
  (if (symbol? (cadr exp))
      (caddr exp)
      (make-lambda (cdadr exp)
                   (cddr exp))))

;; lambda 式は、記号 lambda から始まるリストです。
(define (lambda? exp) (tagged-list? exp 'lambda))
(define (lambda-parameters exp) (cadr exp))
(define (lambda-body exp) (cddr exp))

(define (make-lambda parameters body)
  (cons 'lambda (cons parameters body)))

;; 条件式は if で始まり、述語・結果式・（オプションで）代替式を持ちます。式が代替式部分を持たない場合は、代替式として false を使います。
(define (if? exp) (tagged-list? exp 'if))
(define (if-predicate exp) (cadr exp))
(define (if-consequent exp) (caddr exp))
(define (if-alternative exp)
  (if (not (null? (cdddr exp)))
      (cadddr exp)
      'false))

(define (make-if predicate consequent alternative)
  (list 'if predicate consequent alternative))

;; begin は、式の列を単独の式のバージョン化します。構文演算として、begin 式から実際の列を取り出す演算と、列の最初の式と残りの式を返すセレクタを用意します。
(define (begin? exp) (tagged-list? exp 'begin))
(define (begin-actions exp) (cdr exp))
(define (last-exp? seq) (null? (cdr seq)))
(define (first-exp seq) (car seq))
(define (rest-exps seq) (cdr seq))
(define (sequence->exp seq)
  (cond ((null? seq) seq)
        ((last-exp? seq) (first-exp seq))
        (else (make-begin seq))))
(define (make-begin seq) (cons 'begin seq))

;; 手続きの適用は、ここまで挙げた式の型に含まれない任意の複合式です。
;; 式の car は演算子で、cdr は被演算子のリストです。
(define (application? exp) (pair? exp))
(define (operator exp) (car exp))
(define (operands exp) (cdr exp))
(define (no-operands? ops) (null? ops))
(define (first-operand ops) (car ops))
(define (rest-operands ops) (cdr ops))

;; 派生式
(define (cond? exp) (tagged-list? exp 'cond))
(define (cond-clauses exp) (cdr exp))
(define (cond-else-clause? clause)
  (eq? (cond-predicate clause) 'else))
(define (cond-predicate clause) (car clause))
(define (cond-actions clause) (cdr clause))
(define (cond->if exp) (expand-clauses (cond-clauses exp)))
(define (expand-clauses clauses)
  (if (null? clauses)
      'false
      (let ((first (car clauses))
            (rest (cdr clauses)))
        (if (cond-else-clause? first)
            (if (null? rest)
                (sequence->exp (cond-actions first))
                (error "ELSE clause isn't last: COND->IF"
                     clauses))
            (make-if (cond-predicate first)
                     ;; 練習問題 4.5
                     ; (sequence->exp (cond-actions first))
                     (let ((actions (cond-actions first))
                           (predicate (cond-predicate first)))
                          (if (eq? (car actions) '=>)
                              (list (cadr actions) predicate)
                              (sequence->exp actions)))
                     (expand-clauses rest))))))

; 練習問題 4.6
(define (let? exp) (tagged-list? exp 'let))
(define (let-values exp) (map car (cadr exp)))
(define (let-variables exp) (map cadr (cadr exp)))
(define (let-body exp) (cddr exp))
(define (let->combination exp)
  ; 練習問題 4.8
  ;; (cons (make-lambda (let-values exp)
  ;;                  (let-body exp))
  ;;     (let-variables exp)))
  (if (pair? (cadr exp))
      (cons (make-lambda (let-values exp)
                         (let-body exp))
            (let-variables exp))
      (letn exp)))

; 練習問題 4.7
(define (let*? exp) (tagged-list? exp 'let*))
(define (let*-binds exp) (cadr exp))
(define (let*-body exp) (caddr exp))
(define (let*->nested-lets exp)
  (define (iter binds body)
    (if (null? binds)
        body
        (list 'let (list (car binds)) (iter (cdr binds) body))))
  (iter (let*-binds exp) (let*-body exp)))

; 練習問題 4.8
(define (letn? exp) (tagged-list? exp 'letn))
(define (letn-name exp) (cadr exp))
(define (letn-binds exp) (caddr exp))
(define (letn-vars exp) (map car (letn-binds exp)))
(define (letn-vals exp) (map cadr (letn-binds exp)))
(define (letn-body exp) (cadddr exp))
(define (letn exp)
  (make-begin (list (list 'define (cons (letn-name exp)
                                  (letn-vars exp))
                            (letn-body exp))
                    (cons (letn-name exp) (letn-vals exp)))))

; 練習問題 4.9 (do)
(define (do? exp) (tagged-list? exp 'do))
(define (do-clause exp) (cadr exp))
(define (do-var exp) (car (do-clause exp)))
(define (do-val exp) (cadr (do-clause exp)))
(define (do-end exp) (caddr (do-clause exp)))
(define (do-diff exp) (cadddr (do-clause exp)))
(define (do-body exp) (cddr exp))
(define (do->let exp)
  (list 'let 'iter
             (list (list (do-var exp) (do-val exp))
                   (list 'count (do-end exp)))
             (make-begin (append (do-body exp)
                                 (list (list 'if (list '<= 'count 0)
                                                 'false
                                                 (list 'iter (list '+ (do-var exp) (do-diff exp))
                                                             (list '- 'count (do-diff exp)))))))))

;; 4.1.3 評価器のデータ構造
;; 述語のテスト
(define (true? x) (not (eq? x false)))
(define (false? x) (eq? x false))

;; 手続きの表現
(define (make-procedure parameters body env)
  ; (list 'procedure parameters body env))
  ; 練習問題 4.16.b
  (list 'procedure parameters (scan-out-defines body) env))
(define (compound-procedure? p)
  (tagged-list? p 'procedure))
(define (procedure-parameters p) (cadr p))
(define (procedure-body p) (caddr p))
(define (procedure-environment p) (cadddr p))

;; 環境に対する演算
(define (enclosing-environment env) (cdr env))
(define (first-frame env) (car env))
(define the-empty-environment '())

; 練習問題 4.11
;; (define (make-frame variables values)
;;   (cons variables values))
;; (define (frame-variables frame) (car frame))
;; (define (frame-values frame) (cdr frame))
;; (define (add-binding-to-frame! var val frame)
;;   (set-car! frame (cons var (car frame)))
;;   (set-cdr! frame (cons val (cdr frame))))

(define (make-frame variables values)
  (map cons variables values))
(define (frame-variables frame) (map car frame))
(define (frame-values frame) (map cdr frame))
(define (add-binding-to-frame! var val frame)
  (set-cdr! frame (cons (cons var val) (cdr frame))))
; 練習問題 4.13
(define (remove-binding-from-frame! var frame)
  (cond ((null? frame) (error "Unbound variable is not found:" var))
        ((eq? var (caar frame)) (cdr frame))
        (else (cons (car frame) (remove-binding-from-frame! var (cdr frame))))))

(define (extend-environment vars vals base-env)
  (if (= (length vars) (length vals))
      (cons (make-frame vars vals) base-env)
      (if (< (length vars) (length vals))
          (error "Too many arguments supplied" vars vals)
          (error "Too few arguments supplied" vars vals))))

; 練習問題 4.16.b
; Anser at below has bugs.
; (define (split-definitions-and-exps body)
;   (define (iter body definitions exps)
;     (display body definitions exps)
;     (cond ((null? body)
;            (if (null? definitions)
;                 (list '() body)
;                 (list definitions exps)))
;           ((definition? (car body))
;            (iter (cdr body)
;                  (cons (car body) definitions)
;                  exps))
;           (else
;            (iter (cdr body)
;                  definitions
;                  (cons (car body) exps)))))
;   (iter body '() '()))

; (define (definitions->vars-and-body definitions)
;   (define (iter definitions vars body)
;     (if (null? definitions)
;         (list vars definitions)
;         (iter (cdr definitions)
;               (cons (definition-variable (car definitions)) vars)
;               (cons (definition-value (car definitions)) body))))
;   (iter definitions '() '()))

; (define (render vars body exps)
;   (cons 'let (cons (fold (lambda (x lis)
;                                  (cons (list x ''*unassigned*) lis))
;                           '() vars)
;                    (fold (lambda (x y lis)
;                                  (cons (list 'set! x y) lis))
;                          (fold cons '() exps)
;                          vars body))))

; (define (scan-out-defines procedure-body)
;   (let ((split-ret (split-definitions-and-exps procedure-body)))
;     (let ((ret (definitions->vars-and-body (car split-ret))))
;       (apply-in-underlying-scheme render (list (car ret) (cadr ret) (cadr split-ret))))))

; http://community.schemewiki.org/?sicp-ex-4.16
(define (scan-out-defines body)
  (define (name-unassigned defines)
    (map (lambda (x) (list (definition-variable x) '*unassigned*)) defines))
  (define (set-values defines)
    (map (lambda (x) (list 'set!' (definition-variable x) (definition-value x)))
         defines))
  (define (defines->let exprs defines not-defines)
    (cond ((null? exprs)
           (if (null? defines)
               body
               (list (list 'let (name-unassigned defines)
                                (make-begin (append (set-values defines)
                                                    (reverse not-defines)))))))
          ((definition? (car exprs))
           (defines->let (cdr exprs) (cons (car exprs) defines) not-defines))
          (else (defines->let (cdr exprs) defines (cons (car exprs) not-defines)))))
  (defines->let body '() '()))

; 練習問題 4.12
(define (scan var vars vals)
  (cond ((null? vars) '())
        ((eq? var (car vars)) vals)
        (else (scan var (cdr vars) (cdr vals)))))

(define (lookup-variable-value var env)
  (define (env-loop env)
    ; 練習問題 4.12
    ;; (define (scan vars vals)
    ;;   (cond ((null? vars)
    ;;          (env-loop (enclosing-environment env)))
    ;;         ; 練習問題 4.16.a
    ;;         ; ((eq? var (car vars)) (car vals))
    ;;         ((eq? var (car vars))
    ;;          (let ((val (car vals)))
    ;;            (if (eq? val '*unassigned*)
    ;;                (error "Unassigned Variable" var)
    ;;                val)))
    ;;         (else (scan (cdr vars) (cdr vals)))))
    (if (eq? env the-empty-environment)
        (error "Unbound variable" var)
        (let ((frame (first-frame env)))
          ; 練習問題 4.12
          ;; (scan (frame-variables frame)
          ;;       (frame-values frame)))))
          (let ((result-of-scan (scan var (frame-variables frame) (frame-values frame))))
            (if (null? result-of-scan)
                (env-loop (enclosing-environment env))
                ; 練習問題 4.16.a
                ;; (car result-of-scan))))))
                (if (eq? (car result-of-scan) '*unassigned*)
                    (error "Unassigned variable" var)
                    (car result-of-scan)))))))
  (env-loop env))

(define (set-variable-value! var val env)
  (define (env-loop env)
    ; 練習問題 4.12
    ;; (define (scan vars vals)
    ;;   (cond ((null? vars)
    ;;          (env-loop (enclosing-environment env)))
    ;;         ((eq? var (car vars)) (set-car! vals val))
    ;;         (else (scan (cdr vars) (cdr vals)))))
    (if (eq? env the-empty-environment)
        (error "Unbound variable: SET!" var)
        (let ((frame (first-frame env)))
          (let ((reslt-of-scan (scan var (frame-variables frame) (frame-values frame))))
            (if (null? result-of-scan)
                (env-loop (enclosing-environment env))
                (set-car! result-of-scan val))))))
          ; 練習問題 4.12
          ;; (scan (frame-variables frame)
          ;;       (frame-values frame)))))
  (env-loop env))

(define (define-variable! var val env)
  (let ((frame (first-frame env)))
    ;; (define (scan vars vals)
    ;;   (cond ((null? vars)
    ;;          (add-binding-to-frame! var val frame))
    ;;         ((eq? var (car vars)) (set-car! vals val))
    ;;         (else (scan (cdr vars) (cdr vals)))))
    ;;   (scan (frame-variables frame) (frame-values frame))))
    (let ((result-of-scan (scan var (frame-variables frame) (frame-values frame))))
      (if (null? result-of-scan)
          (add-binding-to-frame! var val frame)
          (set-car! result-of-scan val)))))

; 練習問題 4.13
; make-unbound!が評価されたときの環境からのみはずすべき。なぜなら、より上位の環境から消すと他の部分にも影響が発生するため。
; もし、削除対象の環境から該当する変数が現れない場合は、エラーを出すべき。
(define (unbound? exp) (tagged-list? exp 'unbound))
(define (unbound-var exp) (cadr exp))
(define (eval-unbound exp env)
  (make-unbound! (unbound-var exp) env))
(define (make-unbound! var env)
  (set-car! env (remove-binding-from-frame! var (first-frame env)))
  'ok)
 
;; 4.1.4 評価器をプログラムとして実行する
(define (setup-environment)
  (let ((initial-env
         (extend-environment (primitive-procedure-names)
                             (primitive-procedure-objects)
                             the-empty-environment)))
    (define-variable! 'true true initial-env)
    (define-variable! 'false false initial-env)
    initial-env))

(define (primitive-procedure? proc)
  (tagged-list? proc 'primitive))
(define (primitive-implementation proc) (cadr proc))

(define primitive-procedures
  (list (list 'car car)
        (list 'cdr cdr)
        (list 'cons cons)
        (list 'null? null?)
        (list '+ +)
        (list '- -)
        (list '* *)
        (list '/ /)
        (list '= =)
        (list '<= <=)
        (list 'display display)
        (list 'newline newline)))
(define (primitive-procedure-names)
  (map car primitive-procedures))
(define (primitive-procedure-objects)
  (map (lambda (proc) (list 'primitive (cadr proc)))
       primitive-procedures))

(define (apply-primitive-procedure proc args)
  (apply-in-underlying-scheme
   (primitive-implementation proc) args))

(define input-prompt ";;; M-Eval input:")
(define output-prompt ";;; M-Eval value:")
(define (driver-loop)
  (prompt-for-input input-prompt)
  (let ((input (read)))
    (let ((output (eval input the-global-environment)))
      (announce-output output-prompt)
      (user-print output)))
  (driver-loop))
(define (prompt-for-input string)
  (newline) (newline) (display string) (newline))
(define (announce-output string)
  (newline) (display string) (newline))
(define (user-print object)
  (if (compound-procedure? object)
      (display (llist 'compound-procedure
                      (procedure-parameters object)
                      (procedure-body object)
                      '<procedure-env>))
      (display object)))

(define the-global-environment (setup-environment))
(driver-loop)

